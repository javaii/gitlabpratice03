public class Student{
	int rScore;
	String uniProgram;
	int uniRscore;
	
	public Student(int rS, String uni, int unirS){
		rScore = rS;
		uniProgram = uni;
		uniRscore = unirS;
	}
	
	public String printName(){
		return ("To get into " + uniProgram + ", I would need an R-Score of " + uniRscore);
	}
}