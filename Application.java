public class Application{
	public static void main(String[] args){
		Student a = new Student(31, "Computer Science", 30); 
		Student b = new Student(24, "Civil Engineer", 28);
		
		Student[] section3 = new Student[3];
		section3[0] = a;
		section3[1] = b;
		section3[2] = new Student(22, "Neuroscience", 32);
		
		
		System.out.println(section3[2].printName());
		
	}
}